library(PortfolioAnalytics)
# Specify a portfolio object by passing a character vector for the assets
pspec <- portfolio.spec(assets = manager.names)
#print.default(pspec)

# weight constraints
pspec <- add.constraint(portfolio = pspec,
                        type = "weight_sum",
                        min_sum =0.9999,
                        max_sum =1.0001)
# Box constraints
pspec <- add.constraint(portfolio = pspec,
                        type = "box",
                        min = 0,
                        max = 0.30)

stock.domestic.numb <- match( fund.names.stocks.domestic, manager.names)
stock.foreign.numb <- match( fund.names.stocks.foreign, manager.names)
bond.domestic.numb <- match( fund.names.bond.domestic, manager.names)
mix.domestic.numb <- match( fund.names.mix.domestic, manager.names)

# Add group constraints 
pspec <- add.constraint(portfolio=pspec, type="group",
                        groups = list(groupA = stock.domestic.numb,
                                      groupB = stock.foreign.numb,
                                      groupC = bond.domestic.numb,
                                      groupD = mix.domestic.numb),
                        group_min = c(0.1, 0.1, 0.1, 0),
                        group_max = c(0.7, 0.3, 1, 0.1)
)

# position Limit Constraints
pspec <- add.constraint(portfolio=pspec,
                        type="position_limit", max_pos = 10)

# Target Return constraints
# Add objective to minimize portfolio StdDev with a target of 0.02
pspec  <- add.objective(portfolio=pspec, type="risk", name="StdDev",
                        target=0.02)



pspec <- add.constraint(portfolio=pspec,
                        type="return", return_target = 0.004)

pspec <- add.objective(portfolio=pspec,
                       type="return",
                       name="mean")

pspec <- add.constraint(portfolio=pspec,
                        type="factor_exposure",
                        B = Betas,
                        lower = c(0,    -3,  -3, 0.5, -0.1, -0.1, -0.1),
                        upper = c(0.2, 0.2, 0.2, 0.7,  0.3,  0.3,  0.4)
)
# Factor exposure constraint
# pspec <- add.constraint(portfolio=pspec,
#                         type="factor_exposure", 
#                         B = c(,,,,),
#                         lower = 0.6,
#                         upper = 0.9)
#

# transaction Cost Constraint -> indiviualy adding transaction cost may need 
# pspec <- add.constraint(portfolio=pspec, type = "transaction_cost", ptc = 0.01)
print(pspec)

### ways to specitying constraints as Separate objects
# portfolio risk objective 
# pspec <- add.objective(portfolio=pspec,
#                         type='risk', 
#                        name= 'ETL',
#                        agruments=list(p=0.95))
#install.packages("DEoptim")
#install.packages("slam")
#install.packages("slam",repos="http://r-forge.r-project.org", type = "source")
library(DEoptim)
#install.packages("ROI")
#library(ROI)
fund_returns <- managers.df[, manager.names]
(1+apply(fund_returns,2, mean))^12
opt_maxret <- optimize.portfolio(R=fund_returns, portfolio =pspec,
                                 optimize_method = "DEoptim",
                                 trace=TRUE)