fitfactormodel.residuals <- do.call( cbind, lapply(reg.list, residuals))
n.reboot <- 1000
new.fund.means <- matrix(0, nrow = n.reboot, ncol = length(fund.names))
colnames(new.fund.means) <- fund.names
new.fund.sd <- matrix(0, nrow = n.reboot, ncol = length(fund.names))
colnames(new.fund.sd) <- fund.names

Alphas.mat <- matrix( rep(Alphas, n.boot), nrow = n.boot, ncol = length(fund.names), byrow=TRUE )
for(j in 1 : n.reboot) {
  n.boot <- 5000
  factor.resample.idx <- sample( 1 : nrow( na.omit( factor.fullhistory.df ) ), n.boot, replace = TRUE )
  rand.factors <- factor.fullhistory.df[factor.resample.idx,]
  
  resid.resample.idx <- sample( 1 : nrow(fitfactormodel.residuals), n.boot, replace = TRUE )
  rand.residuals <- fitfactormodel.residuals[resid.resample.idx,]
  #apply(rand.residuals, 2, mean)
  
  
  new.fund.returns <- Alphas.mat + as.matrix( rand.factors ) %*% t(Betas) + as.matrix(rand.residuals) + (1.013^(1/12)-1)
  new.fund.means[j,] <- apply(new.fund.returns,2, mean)
  new.fund.sd[j,] <- apply(new.fund.returns,2, sd)
}